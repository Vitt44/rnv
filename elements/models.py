# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from redactor.fields import RedactorField

class Elements(models.Model):
    title = models.CharField(max_length=300, verbose_name='Заголовок', db_index=True)
    content = RedactorField(blank=True, verbose_name='Контент')

    class Meta:
        verbose_name = 'HTML-элемент'
        verbose_name_plural = 'HTML-элемены'

    def __unicode__(self):
        return self.title
