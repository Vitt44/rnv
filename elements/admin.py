# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Elements


class ElementsAdmin(admin.ModelAdmin):
    list_display = ["title",]
    list_display_links = ["title"]
    exclude = ['title']
    actions = None

    def has_delete_permission(self, request, obj=None):
         if obj is not None:
            return False

    def has_add_permission(self, request, obj=None):
         if obj is not None:
            return False

    class Meta:
        model = Elements


admin.site.register(Elements, ElementsAdmin)
