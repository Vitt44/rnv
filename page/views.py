# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from django.core.mail import send_mail, BadHeaderError
from django.contrib import messages

from .models import Page, Block, MainSlider
from elements.models import Elements
from .forms import MultiForm


def show_page(request, page_url=None):
    footer_element = Elements.objects.get(title='Подвал')
    main_menu = Page.objects.all().order_by('menu_position')
    sidebar_pages = Page.objects.filter(level__lte=2).get_descendants()
    page = get_object_or_404(Page, url=page_url)
    child_pages = Page.objects.filter(parent=page).order_by('-date')
    blocks = Block.objects.filter(pages=page)
    if page.menu_name == 'Главная':
        return redirect('/')
    if request.method == 'POST':
        form = MultiForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            tel = form.cleaned_data['tel']
            mail = form.cleaned_data['mail']
            service = form.cleaned_data['service']
            message = form.cleaned_data['message']
            from_email = 'rabotinavisote@yandex.ru'
            to_email = [from_email, 'vitalii-kuz@yandex.ru']
            subject = ['Обратный звонок', 'Заказ на услугу', 'Сообщение']

            callback_message = "От %s . Контакт для связи: %s" % (name, tel)
            big_message = "От %s тел.: %s email: %s . Сообщение: %s" % (name, tel, mail, message)
            service_message = "От %s тел.: %s интересующая услуга: %s." % (name, tel, service)
            try:
                if message == '':
                    if service == '':
                        send_mail(subject[0], callback_message, from_email, to_email)
                    else:
                        send_mail(subject[1], service_message, from_email, to_email)
                else:
                    send_mail(subject[2], big_message, from_email, to_email)
            except BadHeaderError:
                print "Что-то не так на этапе сообщения"
            messages.success(request, "Сообщение отправлено!")
            return HttpResponseRedirect(page.get_absolute_url())
    else:
        form = MultiForm()
    context = {
        'page': page,
        'sidebar_pages': sidebar_pages,
        'child_pages': child_pages,
        'main_menu': main_menu,
        'blocks': blocks,
        'form': form,
        'footer_element': footer_element
    }
    return render(request, "page.html", context)

def main_page(request):
    footer_element = Elements.objects.get(title='Подвал')
    main_menu = Page.objects.all().order_by('menu_position')
    main_page = Page.objects.get(menu_name='Главная')
    news_list = Page.objects.filter(tag='news').order_by('-date')[:3]
    projects_list = Page.objects.filter(tag='project').order_by('-date')[:3]
    main_slider = MainSlider.objects.all().order_by('position')
    if request.method == 'POST':
        form = MultiForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            tel = form.cleaned_data['tel']
            from_email = 'rabotinavisote@yandex.ru'
            to_email = [from_email, 'vitalii-kuz@yandex.ru']
            subject = 'Обратный звонок'

            callback_message = "От %s . Контакт для связи: %s" % (name, tel)
            try:
                send_mail(subject, callback_message, from_email, to_email)
            except BadHeaderError:
                print "Что-то не так на этапе сообщения"
            messages.success(request, "Сообщение отправлено!")
            return HttpResponseRedirect('/')
    else:
        form = MultiForm()
    context = {
        'main_menu': main_menu,
        'main_page': main_page,
        'news_list': news_list,
        'projects_list': projects_list,
        'main_slider': main_slider,
        'footer_element': footer_element
    }
    return render(request, "main.html", context)
