# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django_mptt_admin.admin import DjangoMpttAdmin

from .models import Page, Block, MainSlider

class PageAdmin(DjangoMpttAdmin):
    exclude = ['url']
    prepopulated_fields = {"page_slug": ("menu_name",)}



class BlockAdmin(admin.ModelAdmin):
    list_display = ["title"]
    list_display_links = ["title"]

    class Meta:
        model = Block


class MainSliderAdmin(admin.ModelAdmin):
    list_display = ["title", "position"]
    list_display_links = ["title"]

    class Meta:
        model = MainSlider



admin.site.register(Page, PageAdmin)
admin.site.register(Block, BlockAdmin)
admin.site.register(MainSlider, MainSliderAdmin)
