from django.conf.urls import url

from .views import show_page, main_page

urlpatterns = [
    url(r'^$', main_page, name='main_page'),
    url(r'^(?P<page_url>[\w\d_/-]+)/$', show_page, name='show_page'),
]
