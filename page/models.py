# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.urls import reverse
from redactor.fields import RedactorField
from mptt.models import MPTTModel, TreeForeignKey


class Page(MPTTModel):
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children', verbose_name='Родительская страница')
    tag = models.CharField(max_length=100, verbose_name='Cвязующий Тэг', blank=True)
    menu_name = models.CharField(max_length=100, verbose_name='Имя в меню', db_index=True)
    menu_position = models.PositiveSmallIntegerField(unique=True, verbose_name='Позиция в меню', default=1)
    visible_in_menu = models.BooleanField(default=False, verbose_name='Видимость в главном меню')
    sidebar_name = models.CharField(max_length=100, verbose_name='Имя в sidebar', blank=True)
    sidebar_position = models.PositiveSmallIntegerField(unique=True, verbose_name='Позиция в sidebar', default=1)
    visible_in_sidebar = models.BooleanField(default=False, verbose_name='Видимость в sidebar')
    bread_name = models.CharField(max_length=100, verbose_name='Имя в крошках', blank=True)
    header_h1 = models.CharField(max_length=300, verbose_name='Заголовок h1', blank=True)
    title = models.CharField(max_length=300, blank=True)
    description = models.CharField(max_length=1000, blank=True)
    keywords = models.CharField(max_length=1000, blank=True)
    preview = models.TextField(max_length=1000, blank=True, verbose_name='Анонс')
    date = models.DateField(auto_now=True)
    image = models.ImageField(upload_to='page_images/', verbose_name='Титульное изображение', null=True, blank=True)
    content_up = RedactorField(blank=True, verbose_name='Контент верхний')
    content_down = RedactorField(blank=True, verbose_name='Контент нижний')
    page_slug = models.SlugField(unique=True, verbose_name='Алиас')
    url = models.CharField(max_length=300)
    service_form = models.BooleanField(default=False, verbose_name='Заказать услугу')
    question_form = models.BooleanField(default=False, verbose_name='Задать вопрос')
    question_form_title = models.CharField(max_length=300, blank=True, verbose_name='Задать вопрос (title)')
    question_form_text = models.TextField(max_length=1000, blank=True, verbose_name='Задать вопрос (text)')

    class Meta:
        verbose_name = 'Страницу сайта'
        verbose_name_plural = 'Страницы сайта'
        #ordering = ["menu_position"]

    def __unicode__(self):
        return self.menu_name

    def save(self, *args, **kwargs):
        try:
            this_record = Page.objects.get(id=self.id)
            if this_record.image != self.image:
                this_record.image.delete(save=False)
        except:
            pass

        if self.parent:
                self.url = '%s/%s' % (self.parent.url, self.page_slug)
        else:
            self.url = self.page_slug
        super(Page, self).save(*args, **kwargs)


    def get_absolute_url(self):
        return reverse('show_page', kwargs={'page_url': self.url})


class Block(models.Model):
    pages = models.ManyToManyField(Page, blank=True, verbose_name='Для страниц')
    title = models.CharField(max_length=300, verbose_name='Имя блока', db_index=True)
    content = RedactorField(blank=True, verbose_name='Контент')

    class Meta:
        verbose_name = 'Блок'
        verbose_name_plural = 'Блоки'

    def __unicode__(self):
        return self.title


class MainSlider(models.Model):
    title = models.CharField(max_length=300, verbose_name='Заголовок')
    image = models.ImageField(upload_to='slider_images/', verbose_name='Изображение')
    position = models.PositiveSmallIntegerField(unique=True, verbose_name='Позиция слайда', default=1)
    url = models.CharField(blank=True, max_length=300, verbose_name='Ссылка слайда')
    text = models.TextField(blank=True, verbose_name='Текст слайда')

    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайды'

    def __unicode__(self):
        return self.title
