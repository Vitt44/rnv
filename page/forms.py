from django import forms

class MultiForm(forms.Form):
    name = forms.CharField(required=False, max_length=50)
    tel = forms.CharField(required=True, max_length=15)
    mail = forms.EmailField(required=False)
    service = forms.CharField(required=False, max_length=50)
    message = forms.CharField(required=False, max_length=360, widget=forms.Textarea)
